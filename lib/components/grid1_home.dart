import 'package:flutter/material.dart';
import 'package:vastr_app/pages/product_details.dart';

import 'categoryList.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {

  var product_list = [
    {
      "name": "FULL SLEEVES",
      "images":"assets/catg/asd1.png",
    },
    {
      "name": "HALF SLEEVES",
      "images":"assets/catg/asd2.png",
    },
    {
      "name": "BASIC",
      "images":"assets/catg/asd1.png",
    },
    {
      "name": "DOODLES",
      "images":"assets/catg/asd3.png",
    },
    {
      "name": "POLO",
      "images":"assets/catg/asd1.png",
    },
  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: product_list.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 8
        ),
        itemBuilder: (BuildContext context, int index){
        return Single_Pro(
          pro_image: product_list[index]['images'],
          pro_name: product_list[index]['name'],
        );
        }
    );
  }
}

class Single_Pro extends StatelessWidget {

  final pro_name;
  final pro_image;

  const Single_Pro(
      {
        this.pro_name,
        this.pro_image
      }
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            width: 120,
            child: Card(
              child: InkWell(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => CategoryList())),
                child: Image.asset(
                  pro_image, fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Container(
            child: Text(
                pro_name,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

class ShirtProduct extends StatefulWidget {
  @override
  _ShirtProductState createState() => _ShirtProductState();
}

class _ShirtProductState extends State<ShirtProduct> {
  var product_list = [
    {
      "name": "FULL SLEEVES",
      "images":"assets/catg/asd1.png"
    },
    {
      "name": "HALF SLEEVES",
      "images":"assets/catg/asd2.png"
    },
    {
      "name": "BASIC",
      "images":"assets/catg/asd3.png"
    },
    {
      "name": "DOODLES",
      "images":"assets/catg/asd4.png"
    },
    {
      "name": "POLO",
      "images":"assets/catg/asd2.png"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 8
        ),
        itemBuilder: (BuildContext context, int index){
          return Single_Pro(
            pro_image: product_list[index]['images'],
            pro_name: product_list[index]['name'],
          );
        }
    );
  }
}

class Single_Pro1 extends StatelessWidget {

  final pro_name;
  final pro_image;

  const Single_Pro1(
      {
        this.pro_name,
        this.pro_image
      }
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            width: 120,
            child: Card(
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.redAccent, width: 2),
                  borderRadius: BorderRadius.circular(25),
                ),
                child: InkWell(
                  onTap: () {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Tap'),
                    ));
                  },
                  child: Image.asset(
                    pro_image, fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Text(
              pro_name,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}


