/// Flutter icons MyFlutterApp
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  MyFlutterApp
///      fonts:
///       - asset: fonts/MyFlutterApp.ttf
///
/// 
/// * Modern Pictograms, Copyright (c) 2012 by John Caserta. All rights reserved.
///         Author:    John Caserta
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://thedesignoffice.org/project/modern-pictograms/
/// * MFG Labs, Copyright (C) 2012 by Daniel Bruce
///         Author:    MFG Labs
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.mfglabs.com/
///
import 'package:flutter/widgets.dart';

class MyFlutterApp {
  MyFlutterApp._();

  static const _kFontFam = 'MyFlutterApp';

  static const IconData user_woman = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData user_male = const IconData(0xf062, fontFamily: _kFontFam);
}
