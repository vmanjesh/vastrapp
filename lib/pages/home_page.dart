import 'dart:convert';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vastr_app/components/grid_home.dart';
import 'package:vastr_app/components/horizontal_list.dart';
import 'package:vastr_app/constant/constants.dart';
import 'package:vastr_app/modelEntity/categoryEntity.dart';
import 'package:vastr_app/pages/fragmanets/myAddress.dart';
import 'package:http/http.dart' as http;
import 'package:vastr_app/pages/fragmanets/termsNconditions.dart';




class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {

  Future<String> _termNCondition(BuildContext context) async{
    return showDialog(
      context: context,
//      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          actions: <Widget>[
            Center(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 0,
                      right: 0,
                        child: Icon(
                            Icons.cancel
                        ),
                    ),
                    Container(
                      height: 500,
                      width: 28,
                    ),
                  ],
                ),
              ),
          ],
        );
    }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.black,
        title: Text('VASTR',
          style: TextStyle(
            fontFamily: 'FunCity',
            fontSize: 25,
            color: Colors.white
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
                Icons.search,
              color: Colors.white,
            ),
            onPressed: () {},
            iconSize: 30,
          ),
          IconButton(
            icon: Icon(
                Icons.favorite_border,
              color: Colors.white,
            ),
            onPressed: () {},
            iconSize: 30,
          ),
          IconButton(
            icon: Icon(
                Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {
//              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(MY_CART);
            },
            iconSize: 30,
          ),
        ],
      ),
      drawer: Drawer(
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 60,
                    color: Colors.grey,
                    padding: EdgeInsets.only(top: 20, left: 25),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.phone_android, color: Colors.white,),
                        SizedBox(width: 15,),
                        Text("Welcome Guest\n Login!", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Container(
                    color: Colors.black,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 13,),
                        GestureDetector(
                          onTap: (){},
                          child: Row(
                            children: <Widget>[
                              SizedBox(width: 15,),
                              Icon(Icons.home, color: Colors.white, size: 30,),
                              SizedBox(width: 15,),
                              Text('Search By Category', style: TextStyle(color: Colors.white, fontSize: 15),)
                            ],
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: (){
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_ADDRESS);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.business, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Addresses', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_ORDER);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.shopping_basket, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Order', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_CART);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.shopping_cart, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Cart', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_PROFILE);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.person, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Profile', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(OFFER_ZONE);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.local_offer, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Offer Zone', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(CUST_SPRT);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.call, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Customer Support', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) => TermsCondition(),
                            );
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.info_outline, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Term and Conditions', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.power_settings_new, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Logout', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 5, right: 5),
        children: <Widget>[
          HorizontalList(),
          _banner,
          SizedBox(height: 10),
          t_shirtLabel,
          SizedBox(height: 10),
          Container(
            height: 250.0,
            child: Products(),
          ),
          shirtLabel,
          SizedBox(height: 10),
          Container(
            height: 250.0,
            child: ShirtProduct(),
          ),
          SizedBox(height: 10),
          favorite_pro,
          SizedBox(height: 10),
          vastr_favorite,
          SizedBox(height: 10),
          HorizontalList2(),
          SizedBox(height: 10),
          also_expose,
        ],
      ),
    );
  }

//  <----------==================Here Define others Widgets==============------------>

  Widget _banner = Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        image: DecorationImage(
          image: NetworkImage('http://www.projects.estateahead.com/vastr/assets/images/image1.png'),
          fit: BoxFit.fill,
        )
    ),
    height: 270,
    /*child: Carousel(
          dotBgColor: Colors.transparent,
          dotColor: Colors.indigoAccent,
          boxFit: BoxFit.contain,
          images: [
            AssetImage('assets/as1.png',),
            AssetImage('assets/as2.png'),
            AssetImage('assets/as3.png'),
            AssetImage('assets/as4.png'),
            AssetImage('assets/as5.png'),
            AssetImage('assets/as6.png'),
          ],
          autoplay: false,
//      animationCurve: Curves.fastOutSlowIn,
//      animationDuration: Duration(milliseconds: 1000),
          dotSize: 4.0,
          indicatorBgPadding: 2.0,
        ),*/

    child: Row(
      children: <Widget>[
        Expanded(
          child: Container(
            color: Colors.transparent,
            child: InkWell(
              onTap: (){},
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.transparent,
            child: InkWell(
              onTap: (){},
            ),
          ),
        )
      ],
    ),
  );

  Widget t_shirtLabel = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey[900], width: 2),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 35,
      child: Center(
          child: Text('T-SIRTS',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22,
            ),
            textAlign: TextAlign.center,
          ),
      ),
    ),
  );
  Widget shirtLabel = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey[900], width: 2),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 35,
      child: Center(
          child: Text('T-SIRTS',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22,
            ),
            textAlign: TextAlign.center,
          ),
      ),
    ),
  );

  Widget vastr_favorite = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey[900], width: 2),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 35,
      child: Center(
          child: Text('VASTR FAVORITES',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
      ),
    ),
  );

Widget also_expose = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey[900], width: 2),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 35,
      child: Center(
          child: Text('ALSO EXPOSE',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
      ),
    ),
  );

  Widget favorite_pro = Container(
    height: 250,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        border: Border.all(color: Colors.indigoAccent),
        image: DecorationImage(
          image: NetworkImage('http://www.projects.estateahead.com/vastr/assets/images/all_banner.png'),
          fit: BoxFit.fill,
        )
    ),
  );
}
