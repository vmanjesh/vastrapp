import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  final _key = UniqueKey();
  var _url = "http://vastr.online/";
  @override
  Widget build(BuildContext context) {
    return WebView(
      key: _key,
      initialUrl: _url,
      javascriptMode: JavascriptMode.unrestricted,
    );
  }
}
